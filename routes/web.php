<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('gallery', ['uses' => 'HomeController@getGallery', 'as' => 'gallery']);
Route::get('blog', ['uses' => 'HomeController@getBlog', 'as' => 'blog']);
Route::get('contact-us', ['uses' => 'HomeController@getContact', 'as' => 'contact']);
Route::get('about-us', ['uses' => 'HomeController@getAbout', 'as' => 'about']);
