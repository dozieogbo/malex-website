<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function getGallery(){
        return view('gallery');
    }

    public function getAbout(){
        return view('about');
    }
}
