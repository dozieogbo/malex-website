<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Malex International Schools</title>

    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

    @yield('styles')

    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

</head>

<body>

<div class="boxed_wrapper">
    @include('partials.header')

   @include('partials.nav')

    @if(isset($page))
    <div class="inner-banner text-center">
        <div class="container">
            <div class="box">
                <h3>{{$page}}</h3>
            </div>
            <div class="breadcumb-wrapper">
                <div class="clearfix">
                    <div class="pull-left">
                        <ul class="list-inline link-list">
                            <li><a href="/">Home</a></li>
                            <li><a href="">{{$page}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @yield('content')

    @include('partials.callout')

    @include('partials.footer')

    <button class="scroll-top tran3s color2_bg">
        <span class="fa fa-angle-up"></span>
    </button>

    {{--<div class="preloader"></div>--}}

    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/menuzord.js')}}"></script>
    <!-- jQuery ui js -->

    {{--<!-- mixit up -->--}}
    {{--<script src="js/jquery.mixitup.min.js"></script>--}}
    {{--<script src="js/jquery.fitvids.js"></script>--}}
    {{--<script src="js/bootstrap-select.min.js"></script>--}}


    <!-- fancy box -->
    {{--<script src="js/jquery.fancybox.pack.js"></script>--}}
    {{--<script src="js/jquery.polyglot.language.switcher.js"></script>--}}
    {{--<script src="js/nouislider.js"></script>--}}
    {{--<script src="js/jquery.bootstrap-touchspin.js"></script>--}}
    {{--<script src="js/SmoothScroll.js"></script>--}}
    {{--<script src="js/jquery.appear.js"></script>--}}
    {{--<script src="js/jquery.countTo.js"></script>--}}
    {{--<script src="js/jquery.flexslider.js"></script>--}}
    {{--<script src="js/imagezoom.js"></script>--}}
    {{--<script id="map-script" src="js/default-map.js"></script>--}}

    @yield('scripts')

    <script src="{{asset('js/custom.js')}}"></script>
</div>

</body>

</html>