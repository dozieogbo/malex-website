<?php $page = 'Gallery' ?>

@extends('layouts.app')

@section('content')
    <section class="latest-project sec-padd four-column">
        <div class="container">

            <div class="row">
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-project">
                            <figure class="imghvr-shutter-in-out-horiz">
                                <img src="images/project/7.jpg" alt="Awesome Image" data-imagezoom="true" />
                                {{--<figcaption>--}}
                                    {{--<div class="content">--}}
                                        {{--<a href="project-single.html">--}}
                                            {{--<h4>Latest Technology</h4>--}}
                                        {{--</a>--}}
                                        {{--<p>Consulting</p>--}}
                                    {{--</div>--}}
                                {{--</figcaption>--}}
                            </figure>
                            <p>Hey there.</p>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/8.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Audit & Assurance</h4>
                                            </a>
                                            <p>Financial</p>
                                        </div>
                                    </figcaption>
                                </figure>
                                <p>Hey there.</p>
                            </div>
                        </div>
                        <!-- /.single-latest-project-carousel -->
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/9.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Business Growth</h4>
                                            </a>
                                            <p>Growth</p>
                                        </div>
                                    </figcaption>
                                </figure>
                                <p>Hey there.</p>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/10.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Transporation Service</h4>
                                            </a>
                                            <p>Marketing</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-project">
                            <figure class="imghvr-shutter-in-out-horiz">
                                <img src="images/project/11.jpg" alt="Awesome Image" />
                                <figcaption>
                                    <div class="content">
                                        <a href="project-single.html">
                                            <h4>Latest Technology</h4>
                                        </a>
                                        <p>Consulting</p>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/12.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Audit & Assurance</h4>
                                            </a>
                                            <p>Financial</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <!-- /.single-latest-project-carousel -->
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/13.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Business Growth</h4>
                                            </a>
                                            <p>Growth</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/14.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Transporation Service</h4>
                                            </a>
                                            <p>Marketing</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="single-project">
                                <figure class="imghvr-shutter-in-out-horiz">
                                    <img src="images/project/15.jpg" alt="Awesome Image" />
                                    <figcaption>
                                        <div class="content">
                                            <a href="project-single.html">
                                                <h4>Latest Technology</h4>
                                            </a>
                                            <p>Consulting</p>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            <ul class="page_pagination center">
                <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                <li><a href="#" class="active tran3s">1</a></li>
                <li><a href="#" class="tran3s">2</a></li>
                <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('js/imagezoom.js')}}"></script>
@endsection