<footer class="main-footer">

    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget">
                                <h3 class="footer-title">About Us</h3>

                                <div class="widget-content">
                                    <div class="text">
                                        <p>The Experts consulting over 20 years of experience we’ll ensure you always
                                            get the best guidance. We serve a clients at every level of their
                                            organization, in whatever capacity we can be most useful, whether
                                            as a trusted advisor.</p>
                                    </div>
                                    <div class="link">
                                        <a href="{{route('about')}}" class="default_link">More About us <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h3 class="footer-title">Quick Links</h3>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="{{route('blog')}}">Visit Blog</a></li>
                                        <li><a href="{{route('gallery')}}">Gallery</a></li>
                                        <li><a href="https://portal.malexschools.com.ng">Check Result</a></li>
                                        <li><a href="{{route('contact')}}">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">


                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget contact-widget">
                                <h3 class="footer-title">Contact us</h3>
                                <div class="widget-content">
                                    <ul class="contact-info">
                                        <li><span class="icon-signs"></span>No. 2A Niger Close, Uwani, Enugu.
                                        </li>
                                        <li><span class="icon-phone-call"></span>+234 806 3747 412</li>
                                        <li><span class="icon-e-mail-envelope"></span>info@malexschools.com</li>
                                    </ul>
                                </div>
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget news-widget">
                                <h3 class="footer-title">Newsletter</h3>
                                <div class="widget-content">
                                    <div class="text">
                                        <p>Sign up today for information on latest news and events.</p>
                                    </div>
                                    <form action="#" class="default-form">
                                        <input type="email" placeholder="Email Address">
                                        <button type="submit" class="thm-btn">Subscribe</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--Footer Bottom-->
    <section class="footer-bottom">
        <div class="container">
            <div class="pull-left copy-text">
                <p>Copyrights © 2017 All Rights Reserved. Powered by <a href="http://www.afrivelle.com"> Afrivelle.</a>
                </p>

            </div>
            <!-- /.pull-right -->
            <div class="pull-right get-text">
                <ul>
                    <li><a href="#">Support | </a></li>
                    <li><a href="#">Privacy & Policy |</a></li>
                    <li><a href="#"> Terms & Conditions</a></li>
                </ul>
            </div>
            <!-- /.pull-left -->
        </div>
        <!-- /.container -->
    </section>

</footer>