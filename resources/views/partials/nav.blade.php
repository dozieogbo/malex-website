<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="main-logo">
                    <a href="/"><img src="{{asset('images/logo/logo.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-md-8 menu-column">
                <nav class="menuzord" id="main_menu">
                    <ul class="menuzord-menu pull-right">
                        <li class="{{isset($page) ? '' : 'active'}}"><a href="/">Home</a></li>

                        <li class="{{isset($page) && $page == 'About Us' ? 'active' : ''}}"><a href="{{route('about')}}">About us</a>
                        </li>

                        <li class="{{isset($page) && $page == 'Gallery' ? 'active' : ''}}"><a href="{{route('gallery')}}">Gallery</a>
                        </li>

                        <li><a href="#">Blog</a>
                        </li>

                        <li><a href="#">Check Result</a>
                        </li>

                        <li><a href="contact.html">Contact us</a></li>
                    </ul>
                    <!-- End of .menuzord-menu -->
                </nav>
                <!-- End of #main_menu -->
            </div>
        </div>
    </div>
    <!-- End of .conatiner -->
</section>