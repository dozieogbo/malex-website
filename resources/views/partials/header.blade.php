<header class="top-bar">
    <div class="container">
        <div class="clearfix">
            <div class="col-left float_left">
                <ul class="top-bar-info">
                    <li><i class="icon-technology"></i>+234 806 3747 412</li>
                    <li><i class="icon-note2"></i>info@malexschools.com</li>
                </ul>
            </div>
            <div class="col-right float_right">
                <ul class="social">
                    <li>Stay Connected:</li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
                <div class="link">
                    <a href="contact.html" class="thm-btn">get a quote</a>
                </div>
            </div>


        </div>


    </div>
</header>